'use strict';

const path = require('path');
const spawn = require('child_process').spawn;

const program = require('commander');
const fs = require('fs-extra');


const trim = str => str.replace(' ', '');
const spawnDefaults = {
	cwd: undefined,
	env: process.env
};

program
	.version('1.0.0')
	.description('Create an RSA keypair')
	.usage('[options]')
	.option('-d, --directory [value]', `Change the output directory`, trim, 'keys')
	.option('-k, --key-name [value]', `Change the output file's name`, trim, 'key')
	.option('-b, --bit-length [value]', `Change the Key's bit length`, trim, '2048')
	.option('-p, --passphrase [value]', `Add a passphrase to your keypair`)
	.parse(process.argv);

const createKeys = (directory, keyName, bitLength, passphrase) => {
	const keyPath = path.join(__dirname, directory);

	return fs.ensureDir(keyPath)
		.then(() => {
			const createPrivateKey = spawn('openssl', ['genrsa', '--out', `${keyPath}/${keyName}`, bitLength], spawnDefaults);
			createPrivateKey.stdout.on('data', data => console.log(data.toString()));
			createPrivateKey.stderr.on('data', data => console.error(data.toString()));
			createPrivateKey.on('error', error => console.error(`[openssl genrsa] ${error}`));

			createPrivateKey.on('close', code => {
				if (code !== 0) return;
				const createpublicKey = spawn('openssl', ['rsa', '-pubout', '-in', `${keyPath}/${keyName}`, '-out', `${keyPath}/${keyName}.pub`], spawnDefaults);
				createPrivateKey.stdout.on('data', data => console.log(data.toString()));
				createpublicKey.stderr.on('data', data => console.error(data.toString()));
				createpublicKey.on('error', innerError => console.error(`[openssl rsa] ${innerError}`));

				createpublicKey.on('close', code => console.log(`child process exited with code ${code}.`));
			});
		})
		.catch(console.error);
};

createKeys(program.directory, program.keyName, program.bitLength, program.passphrase);
