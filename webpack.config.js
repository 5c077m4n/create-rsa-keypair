const path = require('path');

const webpack = require('webpack');


module.exports = {
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'index.bundle.js'
	},
	mode: 'production',
	target: 'node',
	plugins: [
		new webpack.BannerPlugin({ banner: "#!/usr/bin/env node", raw: true })
	]
};
